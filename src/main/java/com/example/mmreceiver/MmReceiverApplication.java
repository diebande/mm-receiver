package com.example.mmreceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmReceiverApplication.class, args);
	}

}
